## [2.0.1](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0...v2.0.1) (2024-02-28)


### Bug Fixes

* **LAB-532:** make the proof's created attribute optional ([c261e77](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/c261e7790ca6f03016e9881cbce17960ac98a352))

## [2.0.1-develop.1](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0...v2.0.1-develop.1) (2024-02-28)


### Bug Fixes

* **LAB-532:** make the proof's created attribute optional ([c261e77](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/c261e7790ca6f03016e9881cbce17960ac98a352))

# [2.0.0](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.0.0...v2.0.0) (2024-02-14)


### Bug Fixes

* add ODRL context to OfflineDocumentLoader ([2c1b308](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/2c1b308fcbfc829e9eb3daf03d22c3bd4d62a574))
* **LAB-486:** fix pipeline ([d03b64d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/d03b64d436cd8edb08cc4f81ed8aed9937cab8d7))
* **LAB-489:** add homepage and repo to package.json ([9c0488e](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/9c0488e2a8ee977533691abb002ab60719f7348f))
* **LAB-489:** enable SonarQube scanning on develop ([9245e2d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/9245e2d7393c767b05a62fa78b5efddc4e76488e))
* **LAB-489:** export components through an index.ts file ([e526878](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/e526878c5ac9b3d59a7640b732d63ad81fbca650))
* **LAB-491:** include sources in the NPM package for source mapping ([4876a30](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/4876a3017a64208e942c96d5f1b65f5cc6600954))
* **LAB-492:** improve the OfflineDocumentLoader to support custom contexts ([6cbd91d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/6cbd91d8b8aca5de2e54ecd67d106bdf5674f38c))
* **LAB-497:** export custom exceptions ([87837cf](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/87837cf39b9cb46f558fa08f7290a50846ebb933))
* **LAB-501:** export JSON-LD related types ([32ad4c7](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/32ad4c71d15eaae14d40731280e38a8316e18ab7))
* remove semantic release CI rule about commit message ([642052f](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/642052fef72911a835bf3d0a3543ea8426872bb1))


### Features

* **LAB-486:** use Vitest as a test framework ([c07c38d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/c07c38db04a18e4c9ef25a1aa9d5effd3a373b3d))
* **LAB-490:** enforce stricter code rules ([9f50f20](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/9f50f204db7a42b49f3b8dfa9e9ff5219fec7fd2))
* **LAB-493:** introduce options to tweak components ([153a5ec](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/153a5ecd558ef07ba45ae9ada4708d0052cd05a5))
* **LAB-495:** move private key arguments to Signer constructor ([8183bcd](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/8183bcdce28f1edb7435bdeee90357b648d90073))
* **LAB-496:** lower required Node version to 18 ([ad32567](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/ad32567220f3b0f1d7ecb8ba049a245bd09afe5a))


### BREAKING CHANGES

* **LAB-495:** the Signer constructor needs private key arguments now.
* **LAB-493:** main methods now use the Options classes instead of multiple arguments.

# [2.0.0-develop.6](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0-develop.5...v2.0.0-develop.6) (2024-02-14)


### Bug Fixes

* remove semantic release CI rule about commit message ([642052f](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/642052fef72911a835bf3d0a3543ea8426872bb1))

# [2.0.0-develop.5](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0-develop.4...v2.0.0-develop.5) (2024-02-14)


### Bug Fixes

* **LAB-501:** export JSON-LD related types ([32ad4c7](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/32ad4c71d15eaae14d40731280e38a8316e18ab7))

# [2.0.0-develop.4](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0-develop.3...v2.0.0-develop.4) (2024-02-13)


### Bug Fixes

* **LAB-497:** export custom exceptions ([87837cf](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/87837cf39b9cb46f558fa08f7290a50846ebb933))

# [2.0.0-develop.3](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0-develop.2...v2.0.0-develop.3) (2024-02-13)


### Bug Fixes

* add ODRL context to OfflineDocumentLoader ([2c1b308](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/2c1b308fcbfc829e9eb3daf03d22c3bd4d62a574))


### Features

* **LAB-496:** lower required Node version to 18 ([ad32567](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/ad32567220f3b0f1d7ecb8ba049a245bd09afe5a))

# [2.0.0-develop.2](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v2.0.0-develop.1...v2.0.0-develop.2) (2024-02-13)


### Features

* **LAB-495:** move private key arguments to Signer constructor ([8183bcd](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/8183bcdce28f1edb7435bdeee90357b648d90073))


### BREAKING CHANGES

* **LAB-495:** the Signer constructor needs private key arguments now.

# [2.0.0-develop.1](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.1.0-develop.5...v2.0.0-develop.1) (2024-02-12)


### Features

* **LAB-493:** introduce options to tweak components ([153a5ec](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/153a5ecd558ef07ba45ae9ada4708d0052cd05a5))


### BREAKING CHANGES

* **LAB-493:** main methods now use the Options classes instead of multiple arguments.

# [1.1.0-develop.5](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.1.0-develop.4...v1.1.0-develop.5) (2024-02-12)


### Bug Fixes

* **LAB-492:** improve the OfflineDocumentLoader to support custom contexts ([6cbd91d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/6cbd91d8b8aca5de2e54ecd67d106bdf5674f38c))

# [1.1.0-develop.4](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.1.0-develop.3...v1.1.0-develop.4) (2024-02-12)


### Bug Fixes

* **LAB-491:** include sources in the NPM package for source mapping ([4876a30](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/4876a3017a64208e942c96d5f1b65f5cc6600954))

# [1.1.0-develop.3](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.1.0-develop.2...v1.1.0-develop.3) (2024-02-07)


### Features

* **LAB-490:** enforce stricter code rules ([9f50f20](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/9f50f204db7a42b49f3b8dfa9e9ff5219fec7fd2))

# [1.1.0-develop.2](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.1.0-develop.1...v1.1.0-develop.2) (2024-02-07)


### Bug Fixes

* **LAB-489:** add homepage and repo to package.json ([9c0488e](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/9c0488e2a8ee977533691abb002ab60719f7348f))
* **LAB-489:** enable SonarQube scanning on develop ([9245e2d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/9245e2d7393c767b05a62fa78b5efddc4e76488e))
* **LAB-489:** export components through an index.ts file ([e526878](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/e526878c5ac9b3d59a7640b732d63ad81fbca650))

# [1.1.0-develop.1](https://gitlab.com/gaia-x/lab/json-web-signature-2020/compare/v1.0.0...v1.1.0-develop.1) (2024-02-07)


### Bug Fixes

* **LAB-486:** fix pipeline ([d03b64d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/d03b64d436cd8edb08cc4f81ed8aed9937cab8d7))


### Features

* **LAB-486:** use Vitest as a test framework ([c07c38d](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/c07c38db04a18e4c9ef25a1aa9d5effd3a373b3d))

# 1.0.0 (2024-02-05)


### Bug Fixes

* **LAB-479:** add .npmrc to repository ([f24755f](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/f24755ffc518eec17610c7c9139a9bc468fcce63))
* **LAB-479:** set publishing access to public ([eed578c](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/eed578c5355941622d5091ebf341d3a43f528226))


### Features

* **LAB-479:** support verifiable credential creation/signature ([6bb4c7a](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/6bb4c7a1279f9aa5b50bd3011ebe5e0c9b453d94))
* **LAB-480:** support verifiable credential verification ([2ba3ad2](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/2ba3ad2039c748ff230e371582ed628de4287c18))

# 1.0.0 (2024-02-05)


### Bug Fixes

* **LAB-479:** add .npmrc to repository ([f24755f](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/f24755ffc518eec17610c7c9139a9bc468fcce63))


### Features

* **LAB-479:** support verifiable credential creation/signature ([6bb4c7a](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/6bb4c7a1279f9aa5b50bd3011ebe5e0c9b453d94))
* **LAB-480:** support verifiable credential verification ([2ba3ad2](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/2ba3ad2039c748ff230e371582ed628de4287c18))

# 1.0.0 (2024-02-05)


### Features

* **LAB-479:** support verifiable credential creation/signature ([6bb4c7a](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/6bb4c7a1279f9aa5b50bd3011ebe5e0c9b453d94))
* **LAB-480:** support verifiable credential verification ([2ba3ad2](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/2ba3ad2039c748ff230e371582ed628de4287c18))

# 1.0.0 (2024-02-05)


### Features

* **LAB-479:** support verifiable credential creation/signature ([6bb4c7a](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/6bb4c7a1279f9aa5b50bd3011ebe5e0c9b453d94))
* **LAB-480:** support verifiable credential verification ([2ba3ad2](https://gitlab.com/gaia-x/lab/json-web-signature-2020/commit/2ba3ad2039c748ff230e371582ed628de4287c18))
