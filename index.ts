export { GaiaXSignatureSigner } from './src/signer/gaia-x-signature.signer'
export { JsonWebSignature2020Signer } from './src/signer/json-web-signature-2020.signer'

export { GaiaXSignatureVerifier } from './src/verifier/gaia-x-signature.verifier'
export { JsonWebSignature2020Verifier } from './src/verifier/json-web-signature-2020.verifier'
export { MalformedProofException } from './src/verifier/exception/malformed-proof.exception'
export { SignatureValidationException } from './src/verifier/exception/signature-validation.exception'
export { UnresolvableVerificationMethodException } from './src/verifier/exception/unresolvable-verification-method.exception'

export { GaiaXSignatureHasher } from './src/hasher/gaia-x-signature.hasher'
export { JsonWebSignature2020Hasher } from './src/hasher/json-web-signature-2020.hasher'

export { OfflineDocumentLoaderBuilder } from './src/jsonld/offline-document.loader'
export * from './src/jsonld/jsonld.types'

export { DidResolver } from './src/resolver/did.resolver'
export { DidResolutionException } from './src/resolver/exception/did-resolution.exception'

export { Proof } from './src/model/proof.model'
export { VerifiableCredential } from './src/model/verifiable-credential.model'
