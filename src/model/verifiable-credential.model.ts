import { Proof } from "./proof.model";

/**
 * Verifiable Credential minimal representation in the sense of the
 * <a href="https://www.w3.org/TR/vc-data-model/">W3C specification</a>.
 */
export interface VerifiableCredential {
  "@context": string[];
  [key: string]: any;
  proof: Proof;
}
