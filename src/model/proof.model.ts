export interface Proof {
  type: 'JsonWebSignature2020'
  created: string
  jws?: string
  proofPurpose: string
  verificationMethod: string
}
