import { KeyLike } from 'jose'

import { Options } from './options'

export interface SignerOptions extends Options {
  /**
   * The private key used to sign the verifiable credential, this is of the {@link KeyLike} type.
   * Please see <a href="https://github.com/panva/jose?tab=readme-ov-file#key-utilities">the jose documentation</a> for
   * more details
   */
  privateKey: KeyLike | Uint8Array

  /**
   * The private key algorithm supported by jose, values can be found
   * <a href="https://github.com/panva/jose/blob/81222d2b5b200a759860172f983b2f6e88e39ca5/src/lib/crypto_key.ts#L45">in this file</a>
   */
  privateKeyAlg: string

  /**
   * The identifier of the public key used to verify the verifiable credential, please see the
   * <a href="https://www.w3.org/community/reports/credentials/CG-FINAL-lds-jws2020-20220721/#json-web-signature-2020">
   *   JsonWebSignature2020 specification</a> for more details.
   */
  verificationMethod: string
}
