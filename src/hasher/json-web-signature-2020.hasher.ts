import { canonize } from 'jsonld'

import { Proof } from '../model/proof.model'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { Hasher } from './hasher'

export class JsonWebSignature2020Hasher extends Hasher {
  async hash(document: Omit<VerifiableCredential, 'proof'>, proof: Proof): Promise<Buffer | Uint8Array> {
    const canonizedData: string = await canonize(document, {
      algorithm: 'URDNA2015',
      format: 'application/n-quads',
      safe: this.safe,
      documentLoader: this.documentLoader
    })

    proof['@context'] = ['https://www.w3.org/2018/credentials/v1', 'https://w3id.org/security/suites/jws-2020/v1']
    delete proof.jws
    const canonizedProof: string = await canonize(proof, {
      algorithm: 'URDNA2015',
      format: 'application/n-quads',
      safe: this.safe,
      documentLoader: this.documentLoader
    })

    const hashedProof: Buffer = this.sha256(canonizedProof).digest()
    const hashedData: Buffer = this.sha256(canonizedData).digest()

    return Buffer.concat([hashedProof, hashedData])
  }
}
