import { canonize } from 'jsonld'

import { VerifiableCredential } from '../model/verifiable-credential.model'
import { Hasher } from './hasher'

export class GaiaXSignatureHasher extends Hasher {
  async hash(document: Omit<VerifiableCredential, 'proof'>): Promise<Buffer | Uint8Array> {
    const canonizedData: string = await canonize(document, {
      algorithm: 'URDNA2015',
      format: 'application/n-quads',
      safe: this.safe,
      documentLoader: this.documentLoader
    })

    return new TextEncoder().encode(this.sha256(canonizedData).digest('hex'))
  }
}
