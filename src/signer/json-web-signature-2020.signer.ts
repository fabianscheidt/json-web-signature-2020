import { JsonWebSignature2020Hasher } from '../hasher/json-web-signature-2020.hasher'
import { SignerOptions } from '../options/signer.options'
import { Signer } from './signer'

/**
 * Implementation of the JSON Web Signature 2020 specification for signing verifiable credentials.
 */
export class JsonWebSignature2020Signer extends Signer {
  constructor(options: SignerOptions) {
    super(new JsonWebSignature2020Hasher(options), options)
  }
}
