import { DIDDocument, JsonWebKey } from 'did-resolver'
import { exportJWK, generateKeyPair } from 'jose'
import { describe, expect, it, vi } from 'vitest'

import { DocumentLoader } from '../jsonld/jsonld.types'
import { OfflineDocumentLoaderBuilder } from '../jsonld/offline-document.loader'
import { VerifiableCredential } from '../model/verifiable-credential.model'
import { DidResolver } from '../resolver/did.resolver'
import { JsonWebSignature2020Verifier } from '../verifier/json-web-signature-2020.verifier'
import { JsonWebSignature2020Signer } from './json-web-signature-2020.signer'

vi.mock('../resolver/did.resolver')

describe('JsonWebSignature2020Signer', () => {
  const document: Omit<VerifiableCredential, 'proof'> = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1'],
    id: 'http://example.edu/credentials/1234',
    type: ['VerifiableCredential', 'AlumniCredential'],
    issuer: 'https://example.edu/issuers/123456',
    issuanceDate: '2024-02-01T15:34:02Z',
    credentialSubject: {
      id: 'did:example:456a9956-fc4b-46c7-bb6e-ba27a9ad0261',
      alumniOf: {
        id: 'did:example:bb578304-9a4f-46dd-9caf-e9b806594b25'
      },
      name: 'Jane Doe'
    }
  }

  const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()

  it.concurrent.each(['EdDSA', 'RS256', 'RS384', 'RS512', 'PS256', 'PS384', 'PS512', 'ES256', 'ES256K', 'ES384', 'ES512'])(
    'should sign a document with a %s private key',
    async privateKeyAlg => {
      const didResolver: DidResolver = new DidResolver()
      const { publicKey, privateKey } = await generateKeyPair(privateKeyAlg)

      vi.spyOn(didResolver, 'resolve').mockImplementation(async (did: string): Promise<DIDDocument> => {
        return Promise.resolve({
          '@context': ['https://www.w3.org/ns/did/v1', 'https://w3id.org/security/suites/jws-2020/v1'],
          id: 'did:web:example.edu',
          verificationMethod: [
            {
              '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
              id: 'did:web:example.edu#key-1',
              type: 'JsonWebKey2020',
              controller: 'did:web:example.edu',
              publicKeyJwk: (await exportJWK(publicKey)) as JsonWebKey
            }
          ],
          assertionMethod: ['did:web:example.edu#key-1']
        })
      })

      const signer: JsonWebSignature2020Signer = new JsonWebSignature2020Signer({
        privateKey,
        privateKeyAlg,
        verificationMethod: 'did:web:example.edu#key-1',
        documentLoader: offlineDocumentLoader
      })
      const verifiableCredential: VerifiableCredential = await signer.sign(document)

      const verifier: JsonWebSignature2020Verifier = new JsonWebSignature2020Verifier({ didResolver, documentLoader: offlineDocumentLoader })
      await verifier.verify(verifiableCredential)
      expect(didResolver.resolve).toHaveBeenCalledWith('did:web:example.edu')
    }
  )
})
