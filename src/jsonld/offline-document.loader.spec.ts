import { documentLoaders } from 'jsonld'
import { MockedFunction, describe, expect, it, vi } from 'vitest'

import CredentialExamplesContext from '../context/credentials_examples_v1_context.json'
import CredentialContext from '../context/credentials_v1_context.json'
import JwsContext from '../context/jws2020_v1_context.json'
import { DocumentLoader } from './jsonld.types'
import { OfflineDocumentLoaderBuilder } from './offline-document.loader'

vi.mock('jsonld', async importOriginal => {
  return {
    ...(await importOriginal<typeof documentLoaders.node>()),
    documentLoaders: {
      node: vi.fn()
    }
  }
})

describe('OfflineDocumentLoader', () => {
  it('should resolve default offline contexts', async () => {
    const mockedDocumentLoader = vi.fn()
    ;(documentLoaders.node as MockedFunction<typeof documentLoaders.node>).mockReturnValue(mockedDocumentLoader)

    const documentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()

    expect(await documentLoader('https://www.w3.org/2018/credentials/v1')).toEqual({
      document: CredentialContext,
      documentUrl: 'https://www.w3.org/2018/credentials/v1'
    })

    expect(await documentLoader('https://www.w3.org/2018/credentials/examples/v1')).toEqual({
      document: CredentialExamplesContext,
      documentUrl: 'https://www.w3.org/2018/credentials/examples/v1'
    })

    expect(await documentLoader('https://w3id.org/security/suites/jws-2020/v1')).toEqual({
      document: JwsContext,
      documentUrl: 'https://w3id.org/security/suites/jws-2020/v1'
    })

    expect(mockedDocumentLoader).not.toHaveBeenCalled()
  })

  it(`should still call the default document loader when the URL isn't managed`, async () => {
    const mockedDocumentLoader = vi.fn(url => ({
      document: 'test value',
      documentUrl: url
    }))
    ;(documentLoaders.node as MockedFunction<typeof documentLoaders.node>).mockReturnValue(mockedDocumentLoader)

    const documentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder().build()

    expect(await documentLoader('https://unmanaged.com/shape-registry/shape')).toEqual({
      document: 'test value',
      documentUrl: 'https://unmanaged.com/shape-registry/shape'
    })

    expect(mockedDocumentLoader).toHaveBeenCalledWith('https://unmanaged.com/shape-registry/shape')
  })

  it('should resolve custom offline contexts', async () => {
    const mockedDocumentLoader = vi.fn()
    ;(documentLoaders.node as MockedFunction<typeof documentLoaders.node>).mockReturnValue(mockedDocumentLoader)

    const myContext = { '@context': [{ my: 'https://my-context.org/2024/entities/v1' }] }
    const customContext = { '@context': [{ c: 'https://custom-context.org/2024/entities/v1' }] }

    const documentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder()
      .addContext('https://my-context.org/2024/entities/v1', myContext)
      .addContext('https://custom-context.org/2024/entities/v1', customContext)
      .build()

    expect(await documentLoader('https://www.w3.org/2018/credentials/v1')).toEqual({
      document: CredentialContext,
      documentUrl: 'https://www.w3.org/2018/credentials/v1'
    })

    expect(await documentLoader('https://www.w3.org/2018/credentials/examples/v1')).toEqual({
      document: CredentialExamplesContext,
      documentUrl: 'https://www.w3.org/2018/credentials/examples/v1'
    })

    expect(await documentLoader('https://w3id.org/security/suites/jws-2020/v1')).toEqual({
      document: JwsContext,
      documentUrl: 'https://w3id.org/security/suites/jws-2020/v1'
    })

    expect(await documentLoader('https://my-context.org/2024/entities/v1')).toEqual({
      document: myContext,
      documentUrl: 'https://my-context.org/2024/entities/v1'
    })

    expect(await documentLoader('https://custom-context.org/2024/entities/v1')).toEqual({
      document: customContext,
      documentUrl: 'https://custom-context.org/2024/entities/v1'
    })

    expect(mockedDocumentLoader).not.toHaveBeenCalled()
  })
})
