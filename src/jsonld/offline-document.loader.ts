import { documentLoaders } from 'jsonld'

import CredentialExamplesContext from '../context/credentials_examples_v1_context.json'
import CredentialContext from '../context/credentials_v1_context.json'
import JwsContext from '../context/jws2020_v1_context.json'
import OdrlContext from '../context/odrl_context.json'
import { DocumentLoader, RemoteDocument } from './jsonld.types'

/**
 * Loads contexts from the filesystem instead of resolving them through the network.
 * <p>
 *   This class is very useful in the case of rate limiting due to a high number of calls or for running tests in a
 *   controllable environment.
 * </p>
 * <p>
 *   To use your own contexts, you can call the {@link addContext} method.
 *
 *   ```typescript
 *   import MyContext from 'contexts/my-context.json'
 *   import CustomContext from 'contexts/custom-context.json'
 *
 *   const offlineDocumentLoader: DocumentLoader = new OfflineDocumentLoaderBuilder()
 *     .addContext('https://my-context.org/2024/entities/v1', MyContext)
 *     .addContext('https://custom-context.org/2024/entities/v1', CustomContext)
 *     .build()
 *   ```
 */
export class OfflineDocumentLoaderBuilder {
  private readonly contexts: Map<string, any> = new Map<string, any>([
    ['https://www.w3.org/2018/credentials/v1', CredentialContext],
    ['https://www.w3.org/2018/credentials/examples/v1', CredentialExamplesContext],
    ['https://www.w3.org/ns/odrl.jsonld', OdrlContext],
    ['https://w3id.org/security/suites/jws-2020/v1', JwsContext]
  ])

  addContext(url: string, context: any) {
    this.contexts.set(url, context)

    return this
  }

  build(): DocumentLoader {
    const nodeDocumentLoader: DocumentLoader = documentLoaders.node()

    return async (url: string): Promise<RemoteDocument> => {
      if (this.contexts.has(url)) {
        return {
          document: this.contexts.get(url),
          documentUrl: url
        }
      }

      return nodeDocumentLoader(url)
    }
  }
}
