export declare interface RemoteDocument {
  contextUrl?: string
  document: string
  documentUrl: string
}

export declare type DocumentLoader = (url: string) => Promise<RemoteDocument>
