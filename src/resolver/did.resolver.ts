import { DIDDocument, DIDResolutionResult, Resolver } from 'did-resolver'
import { getResolver } from 'web-did-resolver'

import { DidResolutionException } from './exception/did-resolution.exception'

export class DidResolver {
  private readonly didResolver = new Resolver({
    ...getResolver()
  })

  /**
   * Resolve the DID document corresponding to the given DID.
   * <p>
   * Only <code>did:web</code> DIDs are supported at the moment
   * </p>
   *
   * @param did the DID to resolve
   * @throws DidResolutionException thrown when the DID cannot be resolved, please see the error message for details
   */
  async resolve(did: string): Promise<DIDDocument> {
    const didResolutionResult: DIDResolutionResult = await this.didResolver.resolve(did)

    if (didResolutionResult.didResolutionMetadata.error) {
      throw new DidResolutionException(did, didResolutionResult.didResolutionMetadata.error)
    }

    return didResolutionResult.didDocument!
  }
}
