export class SignatureValidationException extends Error {
  constructor(documentId: string) {
    super(`The signature of the document with ID ${documentId} cannot be validated, please check the document has not been tampered`)
  }
}
